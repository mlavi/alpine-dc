#!/usr/bin/env python3

import npyscreen, subprocess, re, socket, json, os

class addUserForm(npyscreen.ActionPopup):
    def on_ok(self):
        subprocess.run(['samba-tool', 'user', 'create', self.newUser.value,
                          self.newUserPass.value], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        subprocess.run(['samba-tool', 'group', 'addmembers', self.groups[self.userGroup.value],
                          self.newUser.value], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.newUser.value = None
        self.newUserPass.value = None
        self.userGroup.value = None
        self.display()
        self.parentApp.setNextForm('MAIN')

    def on_cancel(self):
        self.display()
        self.parentApp.setNextForm('MAIN')

    def refreshGroups(self):
        self.groups = []
        self.get_groups = subprocess.Popen(
            ['samba-tool', 'group', 'list'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in self.get_groups.stdout:
            self.groups.append(line.rstrip().decode("utf-8"))

    def create(self):
        self.refreshGroups()
        self.newUser = self.add(npyscreen.TitleText, name='Username:')
        self.newUserPass = self.add(npyscreen.TitleText, name="Password")
        self.userGroup = self.add(npyscreen.TitleCombo, name='Department:', values = self.groups)

class addGroupForm(npyscreen.ActionPopup):
    def on_ok(self):
        subprocess.run(['samba-tool', 'group', 'add', self.newGroup.value], 
                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        toUser = self.parentApp.getForm("USER")
        toUser.refreshGroups()
        toUser.userGroup.values = toUser.groups
        toUser.userGroup.display()
        self.newGroup.value = None
        self.display()
        self.parentApp.setNextForm('MAIN')

    def on_cancel(self):
        self.display()
        self.parentApp.setNextForm('MAIN')

    def create(self):
        self.newGroup = self.add(npyscreen.TitleText, name='Group:')

class changeDomainForm(npyscreen.ActionPopup):
    def on_ok(self):
        self.config['DOMAIN'] = self.newDomain.value.upper().split('.')[0]
        self.config['REALM'] = self.newDomain.value.upper()
        self.config['ADMINPASS'] = self.newPass.value
        with open('/etc/alpinedc/alpinedc.json', 'w') as outfile:
            json.dump(self.config,outfile)
        npyscreen.notify_confirm("Configuration Saved:\n \nRe-initialize Domain for changes to take effect.\nUsers and groups will be imported from default values and any changes that have been made manually will be lost.", editw=1)
        self.newDomain.value = None
        self.newPass.value = None
        self.parentApp.setNextForm('MAIN')

    def on_cancel(self):
        self.parentApp.setNextForm('MAIN')

    def create(self):
        with open('/etc/alpinedc/alpinedc.json') as cfgfile:
            self.config = json.load(cfgfile)
        self.newDomain = self.add(npyscreen.TitleText, name='Domain')
        self.newPass = self.add(npyscreen.TitleText, name='Password')

class homeScreen(npyscreen.FormBaseNewWithMenus):
    def create(self):
        with open('/etc/alpinedc/alpinedc.json') as cfgfile:
            self.config = json.load(cfgfile)
        self.currentfqdn = socket.getfqdn()
        self.currentaddress = socket.gethostbyname(self.currentfqdn)
        self.currentPass = self.config['ADMINPASS']

        self.showFqdn = self.add(npyscreen.TitleFixedText, name='FQDN:', value=self.currentfqdn, editable=False)
        self.showAddress = self.add(npyscreen.TitleFixedText, name='IP Address:', value=self.currentaddress, editable=False)
        self.showPass = self.add(npyscreen.TitleFixedText, name='Password', value=self.currentPass, editable=False)

        self.showHelp = self.add(npyscreen.Pager, relx=6, rely=7,name='Help', autowrap=True, values=['Alpine Domain Controller','','Alpine DC is a Samba 4 Active Directory Domain Controller using the Windows 2008 R2 functional level.','The configuration can be changed by accessing the menu with Ctrl-X. This will allow you to change domain settings, add users and groups, and re-initialize the domain controller.'])

    # The menus are created here.
        self.m1 = self.add_menu(name="Main Menu", shortcut="^M")
        self.m1.addItemsFromList([
            ("Change Config", self.changeConfig),
            ("Re-Initialize Domain",   self.resetDomain),
            ("Add User",   self.whenAddUser),
            ("Add Group",   self.whenAddGroup),
            ("Reboot",   self.whenReboot),
            ("Power Off",   self.whenPowerOff)
        ])

    def resetDomain(self):
        doReset = npyscreen.notify_ok_cancel("Re-Initialize Domain ?", title="Confirm", editw=1)
        if doReset == True:
            npyscreen.notify("Domain Re-Initializing")
            subprocess.call(['rm', '/etc/alpinedc/ip.txt'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            subprocess.call('/etc/local.d/ip-check.start',stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            npyscreen.notify_wait("Domain Re-Initialized")
        with open('/etc/alpinedc/alpinedc.json') as cfgfile:
            self.config = json.load(cfgfile)
        self.showFqdn.value = socket.getfqdn()
        self.showAddress.value = socket.gethostbyname(self.showFqdn.value)
        self.showPass.value = self.config['ADMINPASS']
        self.display()

    def changeConfig(self):
        self.parentApp.setNextForm('CONFIG')
        self.editing = False
        self.parentApp.switchFormNow()

    def whenAddUser(self):
        self.parentApp.setNextForm('USER')
        self.editing = False
        self.parentApp.switchFormNow()

    def whenAddGroup(self):
        self.parentApp.setNextForm('GROUP')
        self.editing = False
        self.parentApp.switchFormNow()
    
    def whenPowerOff(self):
        doPowerOff = npyscreen.notify_ok_cancel(
            "Power Off AlpineDC ?", title="Confirm PowerOff", editw=1)
        if doPowerOff == True:
            subprocess.call(['poweroff'], stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)

    def whenReboot(self):
        doReboot = npyscreen.notify_ok_cancel(
            "Reboot AlpineDC ?", title="Confirm Reboot", editw=1)
        if doReboot == True:
            subprocess.call(['reboot'], stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)

class MyApplication(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm('MAIN', homeScreen, name='Domain Config')
        self.addForm('USER', addUserForm, name='New User')
        self.addForm('GROUP', addGroupForm, name='New Group')
        self.addForm('CONFIG', changeDomainForm, name='Change Config')

if __name__ == '__main__':
    TestApp = MyApplication().run()
    print("All objects, baby.")
