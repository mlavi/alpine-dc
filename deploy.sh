#!/bin/bash

DOMAIN=`cat /etc/alpinedc/alpinedc.json | jq -r '.["DOMAIN"]'`
REALM=`cat /etc/alpinedc/alpinedc.json | jq -r '.["REALM"]'`
ADMINPASS=`cat /etc/alpinedc/alpinedc.json | jq -r '.["ADMINPASS"]'`

CURRENT_IP=`ip addr show eth0 | grep inet\ | awk '{print $2}' | awk -F "/" '{print $1}'`
DOMAINLOWER=`echo "$REALM" | awk '{print tolower($0)}'`
HOSTNAME=`hostname`

function provision {
        echo $CURRENT_IP > /etc/alpinedc/ip.txt
        
        printf "127.0.0.1     localhost.localdomain localhost\n$CURRENT_IP     $HOSTNAME.$DOMAINLOWER $HOSTNAME\n" > /etc/hosts
        printf "search $DOMAINLOWER\nnameserver $CURRENT_IP\n" > /etc/resolv.conf

        if samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm="$REALM" --domain="$DOMAIN" --adminpass="$ADMINPASS"; then
            echo "Re-Provisioned Active Directory"
        else
            echo "Failed to re-provision Active Directory"
        fi    
}

function addgroups {
    INPUT=/etc/alpinedc/groups.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read group
    do
        `samba-tool group add $group` &> /dev/null
    done < $INPUT
    echo "Groups Added"
    IFS=$OLDIFS
}

function addusers {
    INPUT=/etc/alpinedc/users.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read name pass ugroup
    do
        `samba-tool user create $name $pass` &> /dev/null
    done < $INPUT
    echo "Users Added"
    IFS=$OLDIFS
}

function addmemberships {
    INPUT=/etc/alpinedc/users.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read name pass ugroup
    do
        `samba-tool group addmembers $ugroup $name` &> /dev/null
    done < $INPUT
    echo "Users Added to Groups"
    IFS=$OLDIFS
}

function redeploy_domain {
    rc-service samba stop
    rm /etc/samba/smb.conf
    provision
    sed s/^.*dns.*/\ \ \ \ \ \ \ \ dns\ forwarder\ =\ 8.8.8.8/ < /etc/samba/smb.conf > /etc/alpinedc/smb.conf
    mv /etc/alpinedc/smb.conf /etc/samba/smb.conf
    rc-service samba start
    addgroups
    addusers
    addmemberships
}

function deploy_domain {
    provision
    addgroups
    addusers
    addmemberships
}
