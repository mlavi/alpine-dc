#!/bin/bash
##################################################################### 
# Script to install and configure alpinedc on a base
# Alpine Linux installation
#####################################################################

# Update apk and install packages
apk update
apk add samba-dc krb5 jq python3 util-linux
pip3 install npyscreen

# Samba-tool functions
function addgroups {
    INPUT=/etc/alpinedc/groups.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read group
    do
        `samba-tool group add $group` &> /dev/null
    done < $INPUT
    echo "Groups Added"
    IFS=$OLDIFS
}

function addusers {
    INPUT=/etc/alpinedc/users.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read name pass ugroup
    do
        `samba-tool user create $name $pass` &> /dev/null
    done < $INPUT
    echo "Users Added"
    IFS=$OLDIFS
}

function addmemberships {
    INPUT=/etc/alpinedc/users.csv
    OLDIFS=$IFS
    IFS=","
    [ ! -f $INPUT ] &while read name pass ugroup
    do
        `samba-tool group addmembers $ugroup $name` &> /dev/null
    done < $INPUT
    echo "Users Added to Groups"
    IFS=$OLDIFS
}

# Import config from alpinedc.cfg
DOMAIN=`cat ./alpinedc.json | jq -r '.["DOMAIN"]'`
REALM=`cat ./alpinedc.json | jq -r '.["REALM"]'`
ADMINPASS=`cat ./alpinedc.json | jq -r '.["ADMINPASS"]'`
DOMAINLOWER=`echo "$REALM" | awk '{print tolower($0)}'`

# Get current IP address
CURRENT_IP=`ip addr show eth0 | grep inet\ | awk '{print $2}' | awk -F "/" '{print $1}'`

# Get hostname
HOSTNAME=`hostname`

# Modify the /etc/hosts file
printf "127.0.0.1     localhost.localdomain localhost\n$CURRENT_IP     $HOSTNAME.$DOMAINLOWER $HOSTNAME\n" > /etc/hosts

# Remove existing smb.conf
# rm /etc/samba/smb.conf

# Copy new smb.conf over the default
cp ./smb.conf /etc/samba/smb.conf

# Provision the domain controller
samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm=$REALM --domain=$DOMAIN --adminpass=$ADMINPASS

# Configure resolv.conf
printf "search $DOMAINLOWER\nnameserver $CURRENT_IP\n" > /etc/resolv.conf

# Configure kerberos
ln -sf /var/lib/samba/private/krb5.conf /etc/krb5.conf

# Install new init script
cp ./samba /etc/init.d/samba

# Configure the samba service
rc-update add samba
rc-service samba start

# Copy config files to /etc/alpinedc
mkdir /etc/alpinedc
cp ./alpinedc.json /etc/alpinedc/
cp ./users.csv /etc/alpinedc/
cp ./groups.csv /etc/alpinedc/
cp ./interface.py /etc/alpinedc/
cp ./deploy.sh /etc/alpinedc/
touch /etc/alpinedc/ip.txt

# Add service to check for IP change on boot
cp ./ip-check.start /etc/local.d/
rc-update add local default

# Prevent DHCP from overwriting /etc/resolv.conf
mkdir /etc/udhcpc
printf 'RESOLV_CONF="NO"\n' > /etc/udhcpc/udhcpc.conf

# Change root default shell
sed s-/bin/ash-/bin/bash-g < /etc/passwd > ./passwd
mv ./passwd /etc/passwd

# Change inittab for autologin/et   
sed s+^tty1.*+"tty1::respawn:/sbin/agetty -a root 38400 tty1"+ < /etc/inittab > ./inittab
mv ./inittab /etc/inittab

# Start interface on login
echo "/etc/alpinedc/interface.py" > /root/.bash_profile

# Add users and groups
addgroups
addusers
addmemberships

# Restart server to interface
reboot