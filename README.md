# alpine-dc

Instructions and scripts to create an image using alpine linux and samba4 to function as an active directory domain controller for labs.

### Creating a new image from scratch
1. Use the Alpine linux Virtual Machine ISO. It has a slimmed down kernel and is optimized for virtual systems
2. Make sure IPAM is enabled and is providing IP and DNS addresses
3. Use a minimal size vdisk (1GiB should be fine)
4. Boot from the ISO and log in as root (no password)
5. Run `setup-alpine` and configure with the following responses:
   + Select keyboard layout: `none`
   + Enter system hostname: `dc`
   + Which interface to initialize: `eth0`
   + IP address for eth0: `dhcp`
   + Any manual network configurations: `no`
   + Enter new password (will ask to verify)
   + Which timezone are you in: `UTC`
   + Proxy URL: `none`
   + Enter mirror number: `f`
   + Which SSH server: `openssh`
   + Which NTP client `chrony`
   + Which disk to use: `sda`
   + How would you like to use it: `sys`
   + Erase the above disks and continue: `y`
6. After the install completes it will prompt to reboot
7. Run `poweroff`
8. Eject the virtual CD
9. Power on the VM and login as root
10. Install bash and git
  + `apk update`
  + `apk add bash git`
12. Clone configuration files
  + `git clone https://gitlab.com/devnull-42/alpine-dc.git`
13. Run `setup.sh` in the alpine-dc diretory. It will install Samba as well as the automation and configuration files.
14. The VM can be powered off and converted into an image.

### Configuration files
1. Configuration files are located in `/etc/alpinedc`
  + `alpinedc.cfg` can be edited to change the domain and administrator password
  + `groups.csv` contains a list of groups to create when the domain is initialized
  + `users.csv` is a csv containing a list of users to create when the domain is initialized and the groups they should belong to
2. IP check
  + `ip-check.start` is called on boot and checks to see if the IP address has changed. If it has changed then the domain is re-initialized using the settings in `/etc/alpinedc/`

### ToDo
1. Add TUI interface that performs the following:
  + Shows the current config of the domain
  + Option to re-initialize the domain
  + Add A and PTR records to DNS
2. Provide an easy way to upload new `users.csv` and `groups.csv` files
3. Create a reverse DNS zone on creation and re-initialization