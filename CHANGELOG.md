# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0]
### Added
- IP Change Detection
- Auto add users and groups

## [0.9.0] - 2018-04-26
### Added
- Inital release
- Scripts for initial configuration of samba4
- Initial config files
